import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'equalSpacing'
})
export class EqualSpacingPipe implements PipeTransform {

  transform(value: any, spaceGoal?: number): string {
    let valueStr = "" + value;
    if(!spaceGoal){
      spaceGoal = 20;
    }
    const prefix = spaceGoal - valueStr.length;

    for(let i = 0; i<prefix; i++){
      valueStr = valueStr + " ";
    }

    return valueStr;
  }

}
