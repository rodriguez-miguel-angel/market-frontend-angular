import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // template: "<h2>Hello World {{title}}</h2>",
  // styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'market-frontend-angular';
}
