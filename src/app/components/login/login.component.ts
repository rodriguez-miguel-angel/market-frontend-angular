import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";
  message: string = "";

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    // console.log(`username = ${this.username},  password = ${this.password}`);
    this.authService.attemptLogin(this.username, this.password).subscribe((res)=>{
      this.message = "Login successful";
      console.log("successful login");
      // get headers from response
      // console.log(res.headers.get("Authorization"));
      sessionStorage.setItem("token",res.headers.get("Authorization"));
      // navigate to another page
      this.router.navigate(['home']);
    }, 
    (res)=>{
      this.message = res.error.title;
    })
  }

}
