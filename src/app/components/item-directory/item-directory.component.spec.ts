import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDirectoryComponent } from './item-directory.component';

describe('ItemDirectoryComponent', () => {
  let component: ItemDirectoryComponent;
  let fixture: ComponentFixture<ItemDirectoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDirectoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
